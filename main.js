'use strict';
const path = require('path');
const fs = require('fs');
const electron = require('electron');

// Module to control application life.
const app = electron.app

// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

let mainWindow;
let isQuitting = false;

//var ColorWheel = require('colorwheel');

const isAlreadyRunning = app.makeSingleInstance(() => {
  if (mainWindow) {
    if (mainWindow.isMinimized()) {
      mainWindow.restore();
    }

    mainWindow.show();
  }
});

if (isAlreadyRunning) {
  app.quit();
}


function createWindow() {
  // const userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1';
  const userAgent = '(Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36';
  const rammeDesktopIcon = path.join(__dirname, 'images/Icon.png');
  const maxWidthValue = 3000;
  const minWidthValue = 460;
  const maxHeighthValue = 1000;
  const minHeightValue = 800;
  const lastWindowState = {
    width: 460,
    height: 800
  };

  //const darkModeBackgroundColor = '#192633';
  // const darkModeBackgroundColor = '#212021';
  const darkModeBackgroundColor = '#2c3d51';


  const isDarkMode = true;

  // Create the browser window.
  const win = new BrowserWindow({
    title: app.getName(),
    show: true,
    x: lastWindowState.x,
    y: lastWindowState.y,
    minWidth: minWidthValue,
    maxWidth: maxWidthValue,
    minHeight: minHeightValue,
    maxHeight: maxHeighthValue,
    width: lastWindowState.width,
    height: lastWindowState.height,
    maximizable: false,
    fullscreenable: false,
    icon: process.platform === 'linux' && rammeDesktopIcon,
    titleBarStyle: 'hidden-inset',
    darkTheme: isDarkMode,
    backgroundColor: isDarkMode ? darkModeBackgroundColor : '#fff',
    autoHideMenuBar: true,
    webPreferences: {
      preload: path.join(__dirname, 'browser.js'),
      nodeIntegration: false
    }
  });

  win.webContents.setUserAgent(userAgent);

  // and load the index.html of the app.
  win.loadURL(`file://${__dirname}/index.html`)

  win.on('close', e => {
    if (!isQuitting) {
      e.preventDefault();
      //app.quit();

      if (process.platform === 'darwin') {
        app.hide();
      } else {
        win.hide();
      }
    }
  });

  win.on('page-title-updated', e => {
    e.preventDefault();
    //tray.create(win);
  });

  return win;
}

app.commandLine.appendSwitch('--enable-web-bluetooth');

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  mainWindow = createWindow();


  // Open the DevTools.
  //  mainWindow.webContents.openDevTools()

  const page = mainWindow.webContents;

  page.on('dom-ready', () => {
    page.insertCSS(fs.readFileSync(path.join(__dirname, 'browser.css'), 'utf8'));

    mainWindow.show();
  });

});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
