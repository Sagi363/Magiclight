'use strict';

let ledCharacteristic = null;
var isConnect = false;

var Devices = [];
var ConnectedBulbs = [];
var Characteristics = [];

function connectToBulb(device) {
    let filters = [];
    let filterName = device.name;

    filters.push({ services: [0xffe5] });
    filters.push({ name: filterName });

    navigator.bluetooth.requestDevice(
        {
            filters: filters
        })
        .then(device => {
            console.log('Connecting to GATT Server... (' + device.name + ')');
            return device.gatt.connect();
        })
        .then(server => {
            console.log('Getting Service 0xffe5 - Light control...');
            return server.getPrimaryService(0xffe5);
        })
        .then(service => {
            console.log('Getting Characteristic 0xffe9 - Light control...');
            return service.getCharacteristic(0xffe9);
        })
        .then(characteristic => {
            console.log('All ready!');
            //ledCharacteristic = characteristic;

            if (ConnectedBulbs.indexOf(device) == -1) {
                ConnectedBulbs.push(device.name);
                Characteristics.push(characteristic);
                $('#' + device.name).attr("disabled", false);
                $('#icon_' + device.name).removeClass("fa-spinner fa-pulse fa-fw")
                $('#icon_' + device.name).addClass("fa-lightbulb-o")
                $('#icon_' + device.name).css("color", '#2077f9');
            }

            onConnected();
        })
        .catch(error => {
            $('#' + device.name).attr("disabled", false);
            $('#' + device.name).css("color", '#ff1500');
            $('#' + device.name).prop("checked", false);
            console.log('Argh! ' + error);
        });
}


function onConnected() {
    isConnect = true;
    // document.querySelector('.connect-button').classList.add('hidden');
    // document.querySelector('.color-buttons').classList.remove('hidden');
    // document.querySelector('.mic-button').classList.remove('hidden');
}


///Applications/Utilities/Electron_53.app/Contents/MacOS/Electron
function connect() {
    console.log('Requesting Bluetooth Device...');
    //var sagi = "Test_Bulb1";
    var bulbs = document.getElementById("bulbs_list");
    //sagi = "Test_Bulb2";
    //bulbs.insertAdjacentHTML('beforeend', '<input type="checkbox" id="' + sagi + '" value="' + sagi + '"> <i class="fa fa-lightbulb-o" style="margin-right: 10px; font-size: 1.6em;" aria-hidden="true"></i> ' + sagi + '<br>');

    //filters: [{ services: [0xffe5] }]
    navigator.bluetooth.requestDevice(
        {
            filters: [{ services: [0xffe5] }]
        })
        .then(device => {
            console.log('> Found ' + device.name);

            // Save the bluetooth device
            if (Devices.indexOf(device.name) == -1) {
                Devices.push(device.name);

                // Show the device in the list
                var bulbs = document.getElementById("bulbs_list");
                bulbs.insertAdjacentHTML('beforeend', '<input type="checkbox" class="checkbox_bulb" id="' + device.name + '" value="' + device.name + '"> <i id="icon_' + device.name + '" class="fa fa-lightbulb-o" style="margin-right: 10px; font-size: 1.6em;" aria-hidden="true"></i> ' + device.name + '<br>');

                $(".checkbox_bulb").change(function () {
                    if ($(this).is(":checked")) {
                        $('#' + device.name).attr("disabled", true);
                        $('#icon_' + device.name).removeClass("fa-lightbulb-o")
                        $('#icon_' + device.name).addClass("fa-spinner fa-pulse fa-fw")
                        connectToBulb(device);
                    }
                    else {
                        if (ConnectedBulbs.indexOf(device) != -1) {
                            // dict.push({
                            //     key:   device.name,
                            //     value: "the value"
                            // });
                            //ConnectedBulbs.pop();
                            //Characteristics.pop();
                            $('#icon_' + device.name).css("color", '#000');
                        }
                    }
                });
            }
        });
}

function setColor(red, green, blue) {
    let data = new Uint8Array([0x56, red, green, blue, 0x00, 0xf0, 0xaa]);
    Characteristics.forEach(function (characteristic) {
        return characteristic.writeValue(data)
            .catch(err => {
                console.log('Error when writing value! ', err);
                console.log('Reconnecting...');
                ConnectedBulbs.forEach(function(device) {
                    connectToBulb(device);
                }, this);
            });
    }, this);
}

function red() {
    return setColor(255, 0, 0)
        .then(() => console.log('Color set to Red'));
}

function green() {
    return setColor(0, 255, 0)
        .then(() => console.log('Color set to Green'));
}

function blue() {
    return setColor(0, 0, 255)
        .then(() => console.log('Color set to Blue'));
}

function listen() {
    annyang.start({ continuous: true });
}

// Voice commands
// annyang.addCommands({
//     'red': red,
//     'green': green,
//     'blue': blue
// });

// Install service worker - for offline support
if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('serviceworker.js');
}
