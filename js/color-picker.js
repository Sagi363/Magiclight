$(function () {
    var bCanPreview = true;
    var mouseIsDown = false;
    var lastX = 0;
    var lastY = 0;

    var canvasOffset = $("#picker").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;

    // create canvas and context objects
    var canvas = document.getElementById('picker');
    var ctx = canvas.getContext('2d');

    var cursor = makeCursor(180, 62, 30);

    var bool = false;

    // drawing active image
    var image = new Image();
    //image.crossOrigin = "Anonymous";
    image.onload = function () {
        bool = true;
        drawCursor(cursor);
        /*localStorage.setItem("savedImageData", canvas.toDataURL("image/png"));
        if ( img.complete || img.complete === undefined ) {
            image.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
            image.src = src;
        }*/
    }

    $("#picker").mousedown(function (e) {
        handleMouseDown(e);
    });
    $("#picker").mousemove(function (e) {
        handleMouseMove(e);
    });
    $("#picker").mouseup(function (e) {
        handleMouseUp(e);
    });

    // select desired colorwheel
    var imageSrc = 'images/colorwheel_sagi.png';
    image.src = imageSrc;

    $('#picker').mousemove(function (e) { // mouse move handler
        if (bCanPreview) {


            // update controls
            //$('#rVal').val(pixel[0]);
            //$('#gVal').val(pixel[1]);
            //$('#bVal').val(pixel[2]);
            //$('#rgbVal').val(pixel[0]+','+pixel[1]+','+pixel[2]);

            //var dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
            //$('#hexVal').val('#' + ('0000' + dColor.toString(16)).substr(-6));

            //var dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
            //dColor = val('#' + ('0000' + dColor.toString(16)).substr(-6));
            //print('dColor: ' + dColor);

            /* if (dColor !== '#000000') {
                cursor.x = canvasX;
                cursor.y = canvasY;
                drawCursor(cursor);
            } */
        }
    });
    $('#picker').click(function (e) { // click event handler
        bCanPreview = !bCanPreview;
        //var cursor = new Image();
        var canvasOffset = $(canvas).offset();
        var canvasX = Math.floor(e.pageX - canvasOffset.left);
        var canvasY = Math.floor(e.pageY - canvasOffset.top);
        var size = 30;

        // get coordinates of current position
        var canvasOffset = $(canvas).offset();
        var canvasX = Math.floor(e.pageX - canvasOffset.left);
        var canvasY = Math.floor(e.pageY - canvasOffset.top);

        // get current pixel
        var imageData;

        try {
            imageData = ctx.getImageData(canvasX, canvasY, 1, 1);
        }
        catch (err) {

        }

        var pixel = imageData.data;

        // update preview color
        var pixelColor = "rgb(" + pixel[0] + ", " + pixel[1] + ", " + pixel[2] + ")";
        var colorSum = pixel[0] + pixel[1] + pixel[2];

        if (colorSum > 200 && colorSum < 600) {
            // Save the picker color for the bulb        
            var bulbRed = pixel[0];
            var bulbGreen = pixel[1];
            var bulbBlue = pixel[2];
            var rgbColor = pixelColor;

            // Convert to hexColor
            var dColor = pixel[2] + 256 * pixel[1] + 65536 * pixel[0];
            var hexColor = '#' + ('0000' + dColor.toString(16)).substr(-6);

            // Update the bulb color
            $('#bulb_red').val(bulbRed);
            $('#bulb_green').val(bulbGreen);
            $('#bulb_blue').val(bulbBlue);
            $('#hex_color').val(hexColor);
            $('#rgb_color').val(rgbColor);

            changeBulbColor(bulbRed, bulbGreen, bulbBlue);

            $('#color_preview').css('backgroundColor', hexColor);
            $('#color_preview').css('box-shadow', "0px 0px 170px " + hexColor);

            //$('#bulb_light').css('-webkit-filter', pixelColor);
            //$('#bulb_light').css('filter', pixelColor);


            cursor.x = canvasX;
            cursor.y = canvasY;
            drawCursor(cursor);
        }

    });

    /* $('preview').click(function(e) { // preview click
         $('.colorpicker').fadeToggle("slow", "linear");
         bCanPreview = true;
     }); */

    function makeCursor(x, y, radius) {
        var cursor = {
            x: x,
            y: y,
            radius: radius,
            fill: 'rgba(255, 255, 255, .01)',
            stroke: '#fff'
        }
        return (cursor);
    }

    function drawCursor(cursor) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        if (bool) {
            ctx.drawImage(image, 30, 30, image.width, image.height); // draw the image on the canvas 
        }
        var knobCenterX = 180;
        var knobCenterY = 180;
        var knobRadius = 129;
        var rads = Math.atan2(cursor.y - knobCenterY, cursor.x - knobCenterX);
        var indicatorX = knobRadius * Math.cos(rads) + knobCenterX;
        var indicatorY = knobRadius * Math.sin(rads) + knobCenterY;
        var indicatorRadius = 16;

        ctx.beginPath();
        //ctx.moveTo(cursor.x, cursor.y);
        ctx.arc(indicatorX, indicatorY, indicatorRadius, 0, Math.PI * 2, false);
        ctx.fillStyle = cursor.fill;
        ctx.fill();
        ctx.shadowColor = '#000';
        ctx.shadowBlur = 5;
        ctx.shadowOffsetX = 0;
        ctx.shadowOffsetY = 0;
        ctx.lineWidth = 6;
        ctx.strokeStyle = cursor.stroke;
        ctx.stroke();
        ctx.closePath();
    }

    function handleMouseDown(e) {
        mouseX = parseInt(e.clientX - offsetX);
        mouseY = parseInt(e.clientY - offsetY);

        // mousedown stuff here
        lastX = mouseX;
        lastY = mouseY;
        mouseIsDown = true;
    }

    function handleMouseUp(e) {
        mouseX = parseInt(e.clientX - offsetX);
        mouseY = parseInt(e.clientY - offsetY);

        // mouseup stuff here
        mouseIsDown = false;
    }

    function handleMouseMove(e) {
        if (!mouseIsDown) {
            return;
        }

        mouseX = parseInt(e.clientX - offsetX);
        mouseY = parseInt(e.clientY - offsetY);

        // mousemove stuff here
        drawCursor(cursor);

        if (ctx.isPointInPath(lastX, lastY)) {
            cursor.x += (mouseX - lastX);
            cursor.y += (mouseY - lastY);
            cursor.right = cursor.x + cursor.width;
            cursor.bottom = cursor.y + cursor.height;
        }
        else {
            //console.log('sagi');
        }

        lastX = mouseX;
        lastY = mouseY;
        drawCursor(cursor);
    }
});